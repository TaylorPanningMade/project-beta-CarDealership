import React from 'react';

class ManufacturersList extends React.Component {
    constructor(props){
        super(props);
        this.state={
            manufacturers: [],
        };
    }
    
    async componentDidMount() {
        const URL = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers });
        }
    }
    render() {
        return (
        <table className="table table-success table-striped">
            <thead>
                <tr>
                    <th>Manufacturers</th>
                </tr>
            </thead>
        <tbody>
           {this.state.manufacturers.map((manufacturer) => {
               return (
                    <tr key={manufacturer.href}>
                        <td>{ manufacturer.name }</td>
                    </tr>
            );
        })} 
        </tbody>
      </table>
    );
  }
}

  
  export default ManufacturersList