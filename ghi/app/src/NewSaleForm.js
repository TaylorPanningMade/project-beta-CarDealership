import React from 'react';

class NewSaleForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      automobile: '',
      automobiles: [],
      sales_person:'',
      sales_persons: [],
      customer:'',
      customers:[],
      sale_price:'',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
    this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
    this.handleCustomerChange = this.handleCustomerChange.bind(this);
    this.handleSalePriceChange = this.handleSalePriceChange.bind(this);
    
  }

  
  async componentDidMount() {
    const url = 'http://localhost:8100/api/automobiles/';

    const autoResponse = await fetch(url);
  
    if (autoResponse.ok) {
      const data = await autoResponse.json();
   
      this.setState({ automobiles: data.automobiles });
    }
  
    const url2 = 'http://localhost:8090/api/sales_persons/';

    const salesResponse = await fetch(url2);
   
    if (salesResponse.ok) {
      const data = await salesResponse.json();
    
      this.setState({ sales_persons: data.sales_persons });
    }
  
    const url3 = 'http://localhost:8090/api/customers/';

    const customerResponse = await fetch(url3);
 
    if (customerResponse.ok) {
      const data = await customerResponse.json();

      this.setState({ customers: data.customers });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    console.log(data)
    delete data.automobiles;
    delete data.customers;
    delete data.sales_persons;
    console.log(data)
    
    const newSaleUrl = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    console.log(fetchConfig)
  
    const response = await fetch(newSaleUrl, fetchConfig);
    console.log(response)
    if (response.ok) {
      const newSale = await response.json();
      console.log(newSale);
      this.setState({
        automobile: '',
        sales_person: '',
        customer: '',
        sale_price:'',
      });
    }
  }
  handleAutomobileChange(event){
    const value = event.target.value;
    this.setState({ automobile: value })
  }

  handleSalesPersonChange(event) {
    const value = event.target.value;
    this.setState({ sales_person: value });
  }

  handleCustomerChange(event) {
    const value = event.target.value;
    this.setState({ customer: value });
  }
  handleSalePriceChange(event){
    const value = event.target.value;
    this.setState({ sale_price: value});
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={this.handleSubmit} id="create-sales-form">
              <div className="mb-3">
                <select onChange={this.handleAutomobileChange} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                  <option value="">Choose an automobile</option>
                  {this.state.automobiles ? this.state.automobiles.map(auto => {
                    return (
                      <option key={auto.id} value={auto.vin}>{auto.vin}</option>
                    )
                  }):null}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleSalesPersonChange} value={this.state.sales_person} required name="sales_person" id="sales_person" className="form-select">
                  <option value="">Choose a sales person</option>
                  {this.state.sales_persons ? this.state.sales_persons.map(people => {
                    return (
                      <option key={people.id} value={people.id}>{people.name}</option>
                    )
                  }):null}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleCustomerChange} value={this.state.customer} required name="customer" id="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {this.state.customers ? this.state.customers.map(customer => {
                    return (
                      <option key={customer.id} value={customer.id}>{customer.name}</option>
                    )
                  }):null}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleSalePriceChange} value={this.state.sale_price} placeholder="SalePrice" required type="text" name="sale_price" id="sale_price" className="form-control" />
                <label htmlFor="SalePrice">Sale Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default NewSaleForm;
