import { NavLink } from 'react-router-dom';
import React from 'react'

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/manufacturers">Manufacturers list</NavLink>
                <NavLink className="nav-link" to="/manufacturers/new">Add Manufacturer</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/automobiles">Automobile list</NavLink>
                <NavLink className="nav-link" to="/automobiles/new">New Automobile</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/models">Car Models</NavLink>
              <NavLink className="nav-link" to="/models/new">New Car Models</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/customer">New Customer</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/saleshistory">Employee sales</NavLink>
                <NavLink className="nav-link" to="/salesperson">New Sales Person</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/allsales">All Sales</NavLink>
                <NavLink className="nav-link" to="/newsale">New Sale</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/servicehistory">Service History</NavLink>
                <NavLink className="nav-link" to="/technicians">Enter A Technician</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/appointments">Appointments List</NavLink>
                <NavLink className="nav-link" to="/appointments/new">Enter A Appointment</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
