import React from 'react';

class ModelsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      picture_url: '',
      manufacturer_id: '',
      manufacturers: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
    this.handleManufacturerIDChange = this.handleManufacturerIDChange.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.manufacturers;
    const modelsUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(modelsUrl, fetchConfig);
    if (response.ok) {
      const newManufacturer = await response.json();
      console.log(newManufacturer);
      this.setState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
      });
    }
  }
  handleNameChange(event){
    const value = event.target.value;
    this.setState({name:value})
  }

  handlePictureUrlChange(event) {
    const value = event.target.value;
    this.setState({ picture_url: value });
  }

  handleManufacturerIDChange(event) {
    const value = event.target.value;
    this.setState({ manufacturer_id: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Model</h1>
            <form onSubmit={this.handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePictureUrlChange} value={this.state.picture_url} placeholder="PictureUrl" required type="text" name="image" id="image" className="form-control" />
                <label htmlFor="PictureUrl">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleManufacturerIDChange} value={this.state.manufacturer_id} required name="manufacturer" id="manufacturer" className="form-select">
                  <option value="">Choose a manufacturer</option>
                  {this.state.manufacturers.map(manufacturer => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ModelsForm;
