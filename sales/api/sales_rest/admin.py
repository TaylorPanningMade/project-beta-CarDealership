from django.contrib import admin
from .models import  Automobile, Customer, SalesPerson, SalesRecord

admin.site.register(Automobile)
admin.site.register(Customer)
admin.site.register(SalesPerson)
admin.site.register(SalesRecord)