
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutoVO, Technician, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]


class AutoVOEncoder(ModelEncoder):
    model = AutoVO
    properties = [
        "vin",
        "color",
        "year",
    ]
    def get_extra_data(self, o):
        return {"model": o.model.name}

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "car_owner_name",
        "appointment_date",
        "appointment_time",
        "reason",
        "vin",
        "technician",
        "completed",
    
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
    def get_extra_data(self, o):
        count = AutoVO.objects.filter(vin=o.vin).count()
        return {"Vip": count > 0 }


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician_href = content["technician"]
            technician = Technician.objects.get(name=technician_href)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician ID"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_get_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist or was already deleted"})
    else: # PUT
        try:
            content = json.loads(request.body)
            print(content)
            appointment = Appointment.objects.get(id=pk)
            #technician_id = content["technician"]
            #technician = Technician.objects.get(name=technician_id)
            #content["technician"] = technician
         
            props = ["car_owner_name","appointment_date","appointment_time","reason","vin", "completed"]
            for prop in props:
                print(prop)
                if prop in content:
                    setattr(appointment, prop, content[prop])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_list_autovos(request):
    if request.method == "GET":
        autoVo = AutoVO.objects.all()
        return JsonResponse(
            {"AutoVOs": autoVo},
            encoder=AutoVOEncoder,
        )
    else:
        content = json.loads(request.body)
        autoVO = AutoVO.objects.create(**content)
        return JsonResponse(
            autoVO,
            encoder=AutoVOEncoder,
            safe=False,
        )



@require_http_methods("GET")
def api_get_autovo(request, pk):
    if request.method == "GET":
        try:
            autoVO = AutoVO.objects.get(id=pk)
            return JsonResponse(
                autoVO,
                encoder=AutoVOEncoder,
                safe=False
            )
        except AutoVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_get_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist or was already deleted"})
    else: # PUT
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(id=pk)
         
            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response