from django.db import models
from django.urls import reverse

# Create your models here.
class AutoVO(models.Model):
    vin = models.CharField(max_length=50, unique=True)
    import_href = models.CharField(max_length=100, unique=True, null=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()

class Technician(models.Model):
    name = models.CharField(max_length=30)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return f'{self.name} employee# {self.employee_number}'

    def get_api_url(self):
        return reverse("api_get_technician", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("name",)

class Appointment(models.Model):
    car_owner_name = models.CharField(max_length=30)
    appointment_date = models.DateField(editable=True)
    appointment_time = models.TimeField(editable=True)
    reason = models.CharField(max_length=200)
    vin = models.CharField(max_length=50)
    completed = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
        null="Empty"
    )
    def __str__(self):
        return f'{self.car_owner_name} Appointment time: {self.appointment_time}'

    def get_api_url(self):
        return reverse("api_get_appointment", kwargs={"pk": self.pk})
    class Meta:
        ordering = ("appointment_time",)
